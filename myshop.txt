SQL & Database


//Database name "myshop"


1.Membuat Database

  Create Database myshop;


2.Membuat Table di Dalam Database

    CREATE TABLE users(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    );

    CREATE TABLE items(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    description varchar(255),
    price integer,
    stock integer,
    category_id integer REFERENCES users(id)
    );

    CREATE TABLE users(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    );


3.Memasukkan Data pada Table

    INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES (NULL, 'John Doe', 'john@doe.com', 'john123'),(NULL, 'Jane Doe', 'jane@doe.com', 'jenita123');

    INSERT INTO `categories` (`id`, `name`) VALUES (NULL, 'gadget'), (NULL, 'cloth'), (NULL, 'men'), (NULL, 'women'), (NULL, 'branded');

    INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) VALUES (NULL, 'Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1'), (NULL, 'Uniklooh', '\r\nbaju keren dari brand ternama\r\n', '500000', '50', '2'), (NULL, 'IMHO Watch\r\n', '\r\njam tangan anak yang jujur banget\r\n', '2000000', '10', '1');


4.Mengambil Data dari Database

a. Mengambil data users

    SELECT id,name,email FROM users;

b. Mengambil data items

    SELECT * FROM `items` WHERE price > 1000000;

    SELECT * FROM `items` WHERE name LIKE "uniklo";


5.Mengubah Data dari Database

    UPDATE `items` SET `price` = '2500000' WHERE `items`.`id` = 1;
